 <nav class="main-header navbar navbar-expand navbar-white navbar-light">
     <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Notifications Dropdown Menu -->


{{--    <li class="nav-item dropdown">--}}
{{--        <a class="nav-link" data-toggle="dropdown" href="#">--}}
{{--          <i class="far fa-bell"></i>--}}
{{--          <span class="badge badge-warning navbar-badge">15</span>--}}
{{--        </a>--}}
{{--        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">--}}
{{--          <span class="dropdown-item dropdown-header">15 Notifications</span>--}}
{{--          <div class="dropdown-divider"></div>--}}
{{--          <a href="#" class="dropdown-item">--}}
{{--            <i class="fas fa-envelope mr-2"></i> 4 new messages--}}
{{--            <span class="float-right text-muted text-sm">3 mins</span>--}}
{{--          </a>--}}
{{--          <div class="dropdown-divider"></div>--}}
{{--          <a href="#" class="dropdown-item">--}}
{{--            <i class="fas fa-users mr-2"></i> 8 friend requests--}}
{{--            <span class="float-right text-muted text-sm">12 hours</span>--}}
{{--          </a>--}}
{{--          <div class="dropdown-divider"></div>--}}
{{--          <a href="#" class="dropdown-item">--}}
{{--            <i class="fas fa-file mr-2"></i> 3 new reports--}}
{{--            <span class="float-right text-muted text-sm">2 days</span>--}}
{{--          </a>--}}
{{--          <div class="dropdown-divider"></div>--}}
{{--          <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>--}}
{{--        </div>--}}
{{--      </li>--}}

        <li class="nav-item dropdown user-menu">
            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
                <img src="{{asset('backend/dist/img/user2-160x160.jpg')}}" class="user-image img-circle elevation-2" alt="User Image">
                <span class="d-none d-md-inline">Super Admin</span>
            </a>
            <ul class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <!-- User image -->
                <li class="user-header bg-primary">
                    <img src="{{asset('backend/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">

                    <p>

                        <small>Member since Nov. 2012</small>
                    </p>
                </li>
                <!-- Menu Body -->

                <!-- Menu Footer-->
                <li class="user-footer">

                    <div class="row">
                        <div class="col-md-6">
                            <a href="#" class="btn btn-default btn-flat float-right"><b>My Profile</b></a>
                        </div >

                        <div class="col-md-6">

                            <button   class="btn btn-default btn-flat float-right" type="submit">Sign out</button>

                        </div>

                    </div>






                </li>
            </ul>
        </li>

    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
      <a href="index.php" class="brand-link">
          <img src="{{asset('backend/dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
               style="opacity: .8">
          <span class="brand-text font-weight-light">Bakery</span>
      </a>
    <!-- Sidebar -->
    <div class="sidebar">


      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

          <li class="nav-item">
            <a href="{{ url('admin') }}" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>Dashboard</p>
            </a>
          </li>


            <li class="nav-header">USER ROLES</li>

            <li class="nav-item has-treeview">
                <a href="product_list.php" class="nav-link">
                    <i class="nav-icon fas fa-users"></i>
                    <p>
                        ADMIN USERS
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="invoice_list.php" class="nav-link" style="font-size: 13px;">
                            <i class="nav-icon fas fa-list"></i>
                            <p>List</p>
                        </a>
                    </li>


                </ul>
            </li>

            <li class="nav-header">STORE MANAGEMENT</li>
            <li class="nav-item has-treeview">
                <a href="product_list.php" class="nav-link">
                    <i class="nav-icon fas fa-store"></i>
                    <p>
                        STORES
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="invoice_list.php" class="nav-link" style="font-size: 13px;">
                            <i class="nav-icon fas fa-list"></i>
                            <p>List</p>
                        </a>
                    </li>


                </ul>
            </li>

            <li class="nav-header">PRODUCT MANAGEMENT</li>
            <li class="nav-item has-treeview">
                <a href="product_list.php" class="nav-link">
                    <i class="nav-icon fas fa-cubes"></i>
                    <p>
                        PRODUCTS
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="invoice_list.php" class="nav-link" style="font-size: 13px;">
                            <i class="nav-icon fas fa-list"></i>
                            <p>Product List</p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="invoice_list.php" class="nav-link" style="font-size: 13px;">
                            <i class="nav-icon fas fa-list"></i>
                            <p>Categories</p>
                        </a>
                    </li>


                </ul>
            </li>
          <li class="nav-header">SETTINGS</li>

            <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-cog"></i>
              <p>
                 Master Settings
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Api</p>
                  </a>
                </li>

            </ul>
          </li>





          <li class="nav-header">REPORTS</li>

            <li class="nav-item has-treeview">
                <a href="product_list.php" class="nav-link">
                    <i class="nav-icon fas fa-shopping-cart"></i>
                    <p>
                        SALES
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="invoice_list.php" class="nav-link" style="font-size: 13px;">
                            <i class="nav-icon fas fa-list"></i>
                            <p>Product List</p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="invoice_list.php" class="nav-link" style="font-size: 13px;">
                            <i class="nav-icon fas fa-list"></i>
                            <p>Categories</p>
                        </a>
                    </li>
                </ul>

          <li class="nav-header"></li>
          <li class="nav-item">
            <a href="{{ route('admin.logout') }}" class="nav-link bg-default">
              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p class="text">Signout</p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
